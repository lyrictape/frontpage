import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Lanching Soon | LyricTape</title>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Lanching Soon <div style={{
            color: "#0070f3",
            textDecoration: "none"
          }}>LyricTape</div>
        </h1>

        <p className={styles.description}>
          "Stay hungry, stay foolish"{' '}
          <code className={styles.code}>Steve Jobs</code>
        </p>

      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Copyright{' '} LyricTape
        </a>
      </footer>
    </div>
  )
}
